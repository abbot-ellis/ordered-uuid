"""Simplify import of OrderedUUID class."""

from ordereduuid.ordereduuid import OrderedUUID

__all__ = ['OrderedUUID']
